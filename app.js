/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var finalScore; // roundScore
var scores; // 1 -> first player, 2 -> second player
var activePlayer; // 0 -> first player, 1 -> second player
var gameOver;

startNewGame();


//GET the score-0 html content
var firstPlayerScore = document.querySelector('#score-0').textContent;
console.log(firstPlayerScore);


//btn roll - event listener via anonymous function
document.querySelector('.btn-roll').addEventListener('click', function()
  {
    if(!gameOver)
    {
        //get random number
        var randomNumber = Math.floor(Math.random() * 6) + 1;

        //get dice element
        var diceDOM = document.querySelector('.dice');

        //re-display dice
        diceDOM.style.display='block';

        //display dice picture based on random number role
        diceDOM.src = 'dice-' + randomNumber + '.png';

        //update current player's score if number not 1 
        if(randomNumber !== 1)
            {
                finalScore += randomNumber;
                document.querySelector('#currentPlayer-' + activePlayer).textContent = finalScore;
            }
        else 
            {
                switchPlayers();
            }
        }
            
});

// Save the current score to the array
// Check if user is reached 100 -> game won
document.querySelector('.btn-hold').addEventListener('click', function(){
    
    if(!gameOver)
        {
        //add current score to global score
        scores[activePlayer] += finalScore; 

        //update final score
        document.getElementById('score-' + activePlayer).textContent = scores[activePlayer];

        //get players scores
        var playerScore1 = document.getElementById('score-0').textContent;
        var playerScore2 = document.getElementById('score-1').textContent;

        //check if players have won
        if(scores[activePlayer] >= 20) 
            {
                document.getElementById('name-' + activePlayer).textContent = 'Winner';  
                document.querySelector('.dice').style.display = 'none';
                document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
                document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
                gameOver = true;
            }
        else
            {
                switchPlayers();
            }
        }
});


function switchPlayers()
{
    //reset score
    finalScore = 0;
    
     //switch players - display
   document.querySelector('.player-0-panel').classList.toggle('active');
   document.querySelector('.player-1-panel').classList.toggle('active');
    
    //reset current dice for both players
    document.getElementById('currentPlayer-0').textContent = 0;
    document.getElementById('currentPlayer-1').textContent = 0;
    
    //switch palayers - memory
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    
    //hide dice
    document.querySelector('.dice').style.display = 'none';
}

document.querySelector('.btn-new').addEventListener('click', startNewGame);

function startNewGame()
{
    scores = [0,0];
    activePlayer = 0;
    finalScore = 0;

    //set player 1 and 2's score to 0
    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';

    //set player 1 and 2's dice to 0
    document.getElementById('currentPlayer-0').textContent = '0';
    document.getElementById('currentPlayer-1').textContent = '0';
    
    //hide dice
    document.querySelector('.dice').style.display='none';
    
    //remove winner label and set player's name
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';
    
    //remove active player status
    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    
    //make 1st player active player
    document.querySelector('.player-0-panel').classList.add('active');
    
    gameOver = false;
}















